-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2018 at 04:46 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokemon_mantap`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_dataplayer`
--

CREATE TABLE `table_dataplayer` (
  `IDName` varchar(20) DEFAULT NULL,
  `DamageTotal` int(20) DEFAULT NULL,
  `CurrEXP` int(20) DEFAULT NULL,
  `CurrPokemon` varchar(30) DEFAULT NULL,
  `CurrLevel` int(10) DEFAULT NULL,
  `Step` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_dataplayer`
--

INSERT INTO `table_dataplayer` (`IDName`, `DamageTotal`, `CurrEXP`, `CurrPokemon`, `CurrLevel`, `Step`) VALUES
('nael', 32, 550, 'Pikachu (Default)', 5, 0),
('naelele', 0, 0, NULL, 5, 0),
('natttt', 0, 990, NULL, 5, 0),
('nattttzzz', 0, 660, 'Pikachu (Default)', 5, 0),
('akkak', 0, 0, NULL, 0, 0),
('sad', 0, 0, NULL, 0, 0),
('yeyeye', 0, 220, 'Pikachu (Default)', 5, 0),
('nm', 0, 0, NULL, 0, 0),
('testes', 0, 0, 'Pikachu (Default)', 5, 0),
('natan', 39, 0, 'Pikachu (Default)', 5, 0),
('test', 0, 0, 'Pikachu (Default)', 5, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
