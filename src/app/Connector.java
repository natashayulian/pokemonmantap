/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author natan
 */
public class Connector {

    private static String server = "jdbc:mysql://localhost/pokemon_mantap";
    private static String username = "root";
    private static String password = "";
    private static Connection conn;

    public static Connection getConn() {
        if (conn == null) {
            conn = logOn();
        }
        return conn;
    }

    private static Connection logOn() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            System.out.println("Connected");
            return DriverManager.getConnection(server, username, password);

        } catch (SQLException e) {
            e.printStackTrace(System.err);
            System.out.println("Error : " + e.toString());
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace(System.err);
            System.out.println("jdbc not found");
        }

        return null;
    }

    private static void logOff() {
        try {
            conn.close();
            System.out.println("connection closed");
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

    }
}
