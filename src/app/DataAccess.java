/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import model.DataTrainer;

/**
 *
 * @author natan
 */
public class DataAccess {

    //untuk pertama kali masuk tanpa data orang
    public static void addPlayerStat(DataTrainer dt) {
        String query = "INSERT INTO table_dataplayer VALUES (?,?,?,?,?,?)";

        try {
            PreparedStatement st = Connector.getConn().prepareStatement(query);
            st.setString(1, dt.getNama());
            st.setInt(2, dt.getDamageTotal());
            st.setInt(3, dt.getCurrExp());
            st.setString(4, dt.getCurrPokemon());
            st.setInt(5, dt.getCurrLevel());
            st.setInt(6, dt.getStep());

            st.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //update digunakan jika player save data
    public static void updatePlayerStat(DataTrainer dt) {
        String query = "UPDATE table_dataplayer SET DamageTotal = ?, CurrEXP = ?, CurrPokemon = ?, CurrLevel = ?, Step = ? WHERE IDName = '" + dt.getNama() + "'";

        try {
            PreparedStatement st = Connector.getConn().prepareStatement(query);
            st.setInt(1, dt.getDamageTotal());
            st.setInt(2, dt.getCurrExp());
            st.setString(3, dt.getCurrPokemon());
            st.setInt(4, dt.getCurrLevel());
            st.setInt(5, dt.getStep());

            st.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> loadPlayer() {
        String query = "SELECT IDName FROM table_dataplayer";
        ArrayList<String> listPlayer = new ArrayList<>();

        try {
            PreparedStatement st = Connector.getConn().prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String namaGet = rs.getString("IDName");
                listPlayer.add(namaGet);
                
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listPlayer;
    }

    public static DataTrainer selectTrainer(String nama) {
        String query = "SELECT * FROM table_dataplayer WHERE IDName = '" + nama + "'";
        DataTrainer dt = new DataTrainer();

        try {
            PreparedStatement st = Connector.getConn().prepareStatement(query);
            ResultSet rs = st.executeQuery();

            while(rs.next()){
            dt.setNama(rs.getString("IDName"));
            dt.setCurrExp(rs.getInt("CurrEXP"));
            dt.setCurrLevel(rs.getInt("CurrLevel"));
            dt.setCurrPokemon(rs.getString("CurrPokemon"));
            dt.setDamageTotal(rs.getInt("DamageTotal"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return dt;

    }

}
