package view;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import model.Player;
import static model.Player.updateDataPlayerMisc;
import model.Pokemon;
import model.PokemonMove;
/**
 *
 * @author Jason Tan
 */
public class BattleFrame extends JFrame implements KeyListener {
    MainLogin main;
    public BattleFrame (MainLogin main) {
        this.main = main;
        initComponents();
        
    }
    
    public BattleFrame () {
        initComponents();
    }
    
    private void initComponents() {
        //set Pokemon lvl
        r = new Random();
        int lvl = r.nextInt(Player.pikachu.getLevel() + 2) + 1;
        eevee = new Pokemon(lvl);

        //set Frame
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setSize(1050, 630);
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.BLACK);
        setTitle("Battle Pokemon");
        
        pnlBg = new JPanel();
        pnlBg.setLayout(null);
        pnlBg.setSize(1024,576);
        add(pnlBg);
        
        
        //Batttle Animation
        //quick attack
        BattleAnimation pikAnimation1 = new BattleAnimation();
        pikAnimation1.setIcon(new ImageIcon(resizeImage("img/battle/attack.png", 70, 70)));
        pikAnimation1.setBounds(760,180,70,70);
        pnlBg.add(pikAnimation1);
        
        //thunderbolt
        BattleAnimation pikAnimation2 = new BattleAnimation();
        pikAnimation2.setIcon(new ImageIcon(resizeImage("img/battle/thunderbolt.png",70,100)));
        pikAnimation2.setBounds(750, 150, 70, 100);
        pnlBg.add(pikAnimation2);
        
        //thunder
        BattleAnimation pikAnimation3 = new BattleAnimation();
        pikAnimation3.setIcon(new ImageIcon(resizeImage("img/battle/thunder.png",100,170)));
        pikAnimation3.setBounds(760, 100, 100, 170);
        pnlBg.add(pikAnimation3);
        
        //slam
        BattleAnimation pikAnimation4 = new BattleAnimation();
        pikAnimation4.setIcon(new ImageIcon(resizeImage("img/battle/explosionsmoke.png",30,30)));
        pikAnimation4.setBounds(770, 150, 60, 60);
        pnlBg.add(pikAnimation4);
        
        //enemy animation
        BattleAnimation enemyAnimation1 = new BattleAnimation();
        enemyAnimation1.setIcon(new ImageIcon(resizeImage("img/battle/attack.png", 70, 70)));
        enemyAnimation1.setBounds(220, 320, 70, 70);
        pnlBg.add(enemyAnimation1);
        
        
        lblBg = new JLabel();
        lblBg.setIcon(new ImageIcon(resizeImage("img/battle/background.png", 1024, 576)));
        lblBg.setBounds(0,0,1024,576);
        pnlBg.add(lblBg);
        
        //Battle Commands
        lblBattleFight = new JLabel();
        lblBattleFight.setIcon(new ImageIcon(resizeImage("img/battle/battleFight.png", 1024, 144)));
        lblBattleFight.setLocation(1, 432);
        lblBattleFight.setSize(1024, 144);
        lblBattleFight.setVisible(false);
        lblBg.add(lblBattleFight);
        
        lblOptBox = new JLabel();
        lblOptBox.setIcon(new ImageIcon(resizeImage("img/battle/battleCommand.png", 1024, 144)));
        lblOptBox.setLocation(1, 432);
        lblOptBox.setSize(1024, 144);
        lblBg.add(lblOptBox);
        
        //battleOptions
        lblFight = new JLabel();
        lblFight.setIcon(new ImageIcon(resizeImage("img/battle/Fight.png", 260, 92)));
        lblFight.setLocation(500, 30);
        lblFight.setSize(260, 92);
        lblOptBox.add(lblFight);
        
        lblRun = new JLabel();
        lblRun.setIcon(new ImageIcon(resizeImage("img/battle/Run.png", 260, 92)));
        lblRun.setLocation(765, 30);
        lblRun.setSize(260, 92);
        lblOptBox.add(lblRun);
        
        
        //pokemon
        lblPokemonIcon = new JLabel();
        lblPokemonIcon.setIcon(new ImageIcon(resizeImage("img/pokemon/pikachu.png", 160, 160)));
        lblPokemonIcon.setLocation(150, 272);
        lblPokemonIcon.setSize(160, 160);
        lblBg.add(lblPokemonIcon);
        
        lblPokemonIcon = new JLabel();
        lblPokemonIcon.setIcon(new ImageIcon(resizeImage("img/pokemon/eevee.png", 160, 160)));
        lblPokemonIcon.setLocation(700,140);
        lblPokemonIcon.setSize(160, 160);
        lblBg.add(lblPokemonIcon);
        
        
        //base
        lblBase = new JLabel();
        lblBase.setIcon(new ImageIcon(resizeImage("img/battle/playerbaseEliteA.png", 512, 64)));
        lblBase.setLocation(10,368);
        lblBase.setSize(512, 64);
        lblBg.add(lblBase);
        
        lblEnemyBase = new JLabel();
        lblEnemyBase.setIcon(new ImageIcon(resizeImage("img/battle/enemybaseEliteA.png", 307, 153)));
        lblEnemyBase.setLocation(630,180);
        lblEnemyBase.setSize(307, 153);
        lblBg.add(lblEnemyBase);
        
        
        //Pokemon State
        lblPokemonName = new JLabel("Pikachu    Lv." + Player.pikachu.getLevel());
        lblPokemonName.setFont(new Font("Serif", Font.PLAIN, 20));
        lblPokemonName.setLocation(700, 335);
        lblPokemonName.setSize(160, 80);
        lblBg.add(lblPokemonName);
        
        pikachuHp = Player.pikachu.getHp();
        lblPikachuHp = new JLabel(Player.pikachu.getHp() + "/" + pikachuHp);
        lblPikachuHp.setFont(new Font("Serif", Font.PLAIN, 20));
        lblPikachuHp.setLocation(700, 370);
        lblPikachuHp.setSize(80, 80);
        lblBg.add(lblPikachuHp);
        
        lblBattleStat = new JLabel();
        lblBattleStat.setIcon(new ImageIcon(resizeImage("img/battle/battlePlayerBoxD.png", 400, 74)));
        lblBattleStat.setLocation(624, 358);
        lblBattleStat.setSize(400, 74);
        lblBg.add(lblBattleStat);
        
        lblPokemonName = new JLabel("Eevee    Lv." + eevee.getLevel());
        lblPokemonName.setFont(new Font("Serif", Font.PLAIN, 20));
        lblPokemonName.setLocation(20, 0);
        lblPokemonName.setSize(160, 80);
        lblBg.add(lblPokemonName);
             
        eeveeHp = eevee.getHp();
        lblEeveeHp = new JLabel(eevee.getHp() + "/" + eeveeHp);
        lblEeveeHp.setFont(new Font("Serif", Font.PLAIN, 20));
        lblEeveeHp.setLocation(20, 35);
        lblEeveeHp.setSize(80, 80);
        lblBg.add(lblEeveeHp);
        
        lblBattleStat = new JLabel();
        lblBattleStat.setIcon(new ImageIcon(resizeImage("img/battle/battleFoeBoxD.png", 400, 74)));
        lblBattleStat.setLocation(1, 20);
        lblBattleStat.setSize(400, 74);
        lblBg.add(lblBattleStat);
        
        
        //skill selection
        lblSkill1 = new JLabel("<html><font color='white'>Quick Attack</font></html>", SwingConstants.CENTER);
        lblSkill1.setFont(new Font ("Comic Sans" , Font.PLAIN,40));
        lblSkill1.setLocation(15, 9);
        lblSkill1.setSize(378, 68);
        lblBattleFight.add(lblSkill1);
        
        lblSkill2 = new JLabel("<html><font color='white'>Thunder Bolt</font></html>", SwingConstants.CENTER);
        lblSkill2.setFont(new Font ("Comic Sans" , Font.PLAIN,40));
        lblSkill2.setLocation(395, 9);
        lblSkill2.setSize(378, 68);
        lblBattleFight.add(lblSkill2);
        
        //listrik 2 jari
        lblSkill3 = new JLabel("<html><font color ='white'>Thunder</font></html>", SwingConstants.CENTER);
        lblSkill3.setFont(new Font ("Comic Sans" , Font.PLAIN,40));
        lblSkill3.setLocation(15, 70);
        lblSkill3.setSize(378, 68);
        lblBattleFight.add(lblSkill3);
        
        lblSkill4 = new JLabel("<html><font color ='white'>Slam</font></html>", SwingConstants.CENTER);
        lblSkill4.setFont(new Font ("Comic Sans" , Font.PLAIN,40));
        lblSkill4.setLocation(395, 70);
        lblSkill4.setSize(378, 68);
        lblBattleFight.add(lblSkill4);
        
        lblSelect = new JLabel();
        lblSelect.setIcon(new ImageIcon(resizeImage("img/battle/select.png", 378, 68)));
        lblSelect.setSize(378, 68);
        lblSelect.setVisible(false);
        lblBattleFight.add(lblSelect);
        
        
        //Action Listener
        lblSkill1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Pikachu use Quick Attack");
                System.out.print("Pikachu do : ");
                Player.pikachu.move1(eevee, pikAnimation1);
                
                challengerTurn(enemyAnimation1);
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
                lblSelect.setLocation(8, 9);
                lblSelect.setVisible(true);
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
                lblSelect.setVisible(false);
            }
        });
        
        lblSkill2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Thread t2 = new Thread(pikAnimation2);
                t2.start();
                System.out.println("Pikachu use Thunder Bolt");
                System.out.print("Pikachu do : ");
                Player.pikachu.move2(eevee, pikAnimation2);
                
                challengerTurn(enemyAnimation1);
                
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
                lblSelect.setLocation(390, 9);
                lblSelect.setVisible(true);
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
                lblSelect.setVisible(false);
            }
        });
        
        lblSkill3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Pikachu use Thunder");
                System.out.print("Pikachu do : ");
                Player.pikachu.move3(eevee, pikAnimation3);
                
                challengerTurn(enemyAnimation1);
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
                lblSelect.setLocation(8, 75);
                lblSelect.setVisible(true);
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
                lblSelect.setVisible(false);
            }
        });
        
        lblSkill4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Pikachu use Slam");
                System.out.print("Pikachu do : ");
                Player.pikachu.move4(eevee, pikAnimation4);
                
                challengerTurn(enemyAnimation1);
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
                lblSelect.setLocation(390, 75);
                lblSelect.setVisible(true);
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
                lblSelect.setVisible(false);
            }
        });
        
        lblFight.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                lblBattleFight.setVisible(true);
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
                lblFight.setIcon(new ImageIcon(resizeImage("img/battle/FightSel.png", 260, 96)));
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
                lblFight.setIcon(new ImageIcon(resizeImage("img/battle/Fight.png", 260, 96)));
            }
        });
        
        lblRun.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                clip.stop();
                main.setVisible(true);
                MainLogin.playSound();
                setVisible(false);
                
                int x_challenger = r.nextInt(930) + 21;
                int y_challenger = r.nextInt(501);
                main.challengerPosition(x_challenger, y_challenger);
                
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
                lblRun.setIcon(new ImageIcon(resizeImage("img/battle/RunSel.png", 260, 96)));
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
                lblRun.setIcon(new ImageIcon(resizeImage("img/battle/Run.png", 260, 96)));
            }
        });
        
    }
    
   
    
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.out.println("test");
            lblBattleFight.setVisible(false); 
        }

    }
    
    //Challenger Turn
    private void challengerTurn(BattleAnimation animation) {
        
        lblEeveeHp.setText(eevee.getHp() + "/" + eeveeHp);
        
        System.out.print("Eevee do : ");
        int move = r.nextInt(4) + 1;
        if (move == 1) {
            eevee.move1(Player.pikachu, animation);
        } else if (move == 2) {
            eevee.move2(Player.pikachu, animation);
        } else if (move == 3) {
            eevee.move3(Player.pikachu, animation);
        } else {
            eevee.move4(Player.pikachu, animation);
        }

        lblPikachuHp.setText(Player.pikachu.getHp() + "/" + pikachuHp);

        if (eevee.getHp() == 0 || Player.pikachu.getHp() == 0) {
            if (eevee.getHp() == 0) {
                System.out.println("Player Win");
                
                //exp gained
                int xpGet = (110 * eevee.getLevel());
                System.out.println("Xp get : " + xpGet);
                int maxXp = (120 * Player.pikachu.getLevel());
                Player.pikachu.setCurrXp((Player.pikachu.getCurrXp() + xpGet));
                
                JOptionPane.showMessageDialog(null, "Player Win!" + "\nXp get : " + xpGet);
                while (Player.pikachu.getCurrXp() >= maxXp) {
                    Player.pikachu.setLevel(Player.pikachu.getLevel() + 1);
                    Player.pikachu.setCurrXp(Player.pikachu.getCurrXp() - maxXp);
                    System.out.println("Pikachu Leveled up to lv " + Player.pikachu.getLevel());
                    maxXp = (120 * Player.pikachu.getLevel());
                    JOptionPane.showMessageDialog(null, "LEVEL UP!" + "\nPikachu level: " + Player.pikachu.getLevel());
                }
                System.out.println("Pikachu Exp : " + Player.pikachu.getCurrXp() + "/" + maxXp);
            } else {
                System.out.println("Player Lose");
                JOptionPane.showMessageDialog(null, "Player Lose!");
            }
            clip.stop();
            main.setVisible(true);
            MainLogin.playSound();
            setVisible(false);
                
            Player.pikachu.setHp(pikachuHp);
            int x_challenger = r.nextInt(930) + 21;
            int y_challenger = r.nextInt(501);
            main.challengerPosition(x_challenger, y_challenger);
        }
        System.out.println("");
    }
    
    
    //Image Edit
    private Image resizeImage(String url, int w, int h){
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance (w,h, Image.SCALE_SMOOTH);
        } catch (IOException ex){
            ex.printStackTrace(System.err);
        }
        return dimg;
    }
    
    //main musik
    public static void playSound() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("music/15.wav").getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
}
    
    public static void main(String[] args) {
        BattleFrame battle = new BattleFrame();
        battle.setVisible(true);
    }   
    
    static Clip clip;
    private JPanel pnlBg; 
    private JLabel lblBg;
    private JLabel lblBase;
    private JLabel lblEnemyBase;
    private JLabel lblOptBox;
    private JLabel lblFight;
    private JLabel lblRun;
    private JLabel lblBattleFight;
    private JLabel lblBattleStat;
    private JLabel lblPokemonIcon;
    private JLabel lblPokemonName;
    private JLabel lblPikachuHp;
    private JLabel lblEeveeHp;
    private JLabel lblSkill1;
    private JLabel lblSkill2;
    private JLabel lblSkill3;
    private JLabel lblSkill4;
    private JLabel lblSelect;
    
    //Pokemon
    private Pokemon eevee;
    private int pikachuHp;
    private int eeveeHp;
    
    Random r;
}
