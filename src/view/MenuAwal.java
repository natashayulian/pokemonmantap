/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import app.DataAccess;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import model.DataTrainer;
import model.Player;
import static view.LoginFrame.clip;

/**
 *
 * @author natan
 */
public class MenuAwal extends JFrame {

    public boolean statusMenu = true;
    //tampilan awal

    public MenuAwal() {
        initComponents();
    }

    public void initComponents() {

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        this.setSize(1050, 630);
        setLocationRelativeTo(null);
        setTitle("WelcomeScreen");
        //
        getContentPane().setBackground(Color.red);

        //panel utama ke jframe
        mainPanel = new JPanel();
        add(mainPanel);

        //background ke panel
        bkgImg = new JLabel();
        bkgImg.setIcon(new ImageIcon(resizeImg("img/MenuAwal/bg.png", 1025, 580)));
        mainPanel.add(bkgImg);

        //logo awal pembuka
        logo = new JLabel();
        logo.setIcon(new ImageIcon(resizeImg("img/MenuAwal/mainLogo.png", 500, 210)));
        logo.setBounds(235, 30, 500, 312);
        bkgImg.add(logo);

        //credit kelompok
        credit = new JLabel("Game remake by : Natasha Yulian, Irvandi Gian, Jason Tan, Natanael Kevin");
        credit.setFont(new Font("Calibri", Font.BOLD, 20));
        credit.setLocation(10, 300);
        credit.setSize(1025, 500);
        credit.setForeground(Color.WHITE);
        bkgImg.add(credit);

        //tombol new player dan load
        //button text new
        newGame = new JLabel("NEW GAME");
        newGame.setFont(new Font("Calibri", Font.BOLD, 27));
        newGame.setLocation(430, 260);
        newGame.setSize(200, 80);
        bkgImg.add(newGame);

        //button text load
        loadGame = new JLabel("LOAD");
        loadGame.setFont(new Font("Calibri", Font.BOLD, 27));
        loadGame.setLocation(460, 315);
        loadGame.setSize(200, 80);
        bkgImg.add(loadGame);

        //active button
        activeBtn = new JLabel();
        activeBtn.setIcon(new ImageIcon(resizeImg("img/MainGraph/button/button_menu_select.png", 150, 60)));
        activeBtn.setSize(200, 80);
        activeBtn.setVisible(false);
        bkgImg.add(activeBtn);

        //button new
        newGameButton = new JLabel();
        newGameButton.setIcon(new ImageIcon(resizeImg("img/MainGraph/button/button_menu.png", 150, 60)));
        newGameButton.setLocation(420, 260);
        newGameButton.setSize(200, 80);
        bkgImg.add(newGameButton);

        //button load
        loadGameButton = new JLabel();
        loadGameButton.setIcon(new ImageIcon(resizeImg("img/MainGraph/button/button_menu.png", 150, 60)));
        loadGameButton.setLocation(420, 315);
        loadGameButton.setSize(200, 80);
        bkgImg.add(loadGameButton);

        newGame.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("New Game");
                String name;

                name = JOptionPane.showInputDialog("Enter your name to begin the game! ");

                //cek kalo nama lom diisi, gamasuk
                if (name != null) {
                    setVisible(false);
                    DataTrainer data = new DataTrainer(name);
                    Player.inputDataNewGame(data);
                    statusMenu = false;

                    MainLogin.playSound();
                    MainLogin main = new MainLogin(true);
                    main.setVisible(true);

                    Player.addPlayer();
                    clip.stop();
                }

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                activeBtn.setLocation(420, 260);
                activeBtn.setVisible(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                activeBtn.setVisible(false);

            }
        });

        loadGame.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Load Game");
                JOptionPane.showMessageDialog(null,"Player: \n" + DataAccess.loadPlayer());

                String name = JOptionPane.showInputDialog("Enter your name");
                if (name != null) {
                    DataTrainer dataGet = new DataTrainer();

                    dataGet = DataAccess.selectTrainer(name);

                    Player.loadDataPlayer(dataGet);
                    JOptionPane.showMessageDialog(null, "Data loaded");

                    MainLogin.playSound();
                    MainLogin main = new MainLogin(true);
                    main.setVisible(true);
                    clip.stop();
                } else {
                    JOptionPane.showMessageDialog(null, "Load canceled by user");
                }

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                activeBtn.setLocation(420, 315);
                activeBtn.setVisible(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                activeBtn.setVisible(false);
            }
        });

    }

    //table
    private void showToTable(LinkedList<DataTrainer> listPlayer) {
        arrData = new Object[listPlayer.size()][2];

        int i = 0;
        for (DataTrainer dt : listPlayer) {
            arrData[i][0] = dt.getNama();
            arrData[i][1] = dt.getCurrExp();
            i++;
        }

        DefaultTableModel dtm = new DefaultTableModel(arrData, null);
        tblShow.setModel(dtm);
        spTbl1.setViewportView(tblShow);

    }

    private Image resizeImg(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);

        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }

        return dimg;
    }

    public JPanel mainPanel;
    public JLabel bkgImg;
    public JLabel loadGameButton;
    public JLabel newGameButton;
    public JLabel activeBtn;
    public JLabel newGame;
    public JLabel loadGame;
    public JLabel logo;
    public JLabel credit;
    public Object arrData[][];
    public JTable tblShow;
    public JScrollPane spTbl1;

}
