/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author natan
 */
public class DataTrainer {

    private String nama;
    private int damageTotal;
    private int currExp;
    private int playTime;
    private String currPokemon;
    private int currLevel;
    private int step;

    public DataTrainer(){
        
    }

    public DataTrainer(String name) {
        this.nama = name;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getDamageTotal() {
        return damageTotal;
    }

    public void setDamageTotal(int damageTotal) {
        this.damageTotal = damageTotal;
    }

    public int getCurrExp() {
        return currExp;
    }

    public void setCurrExp(int currExp) {
        this.currExp = currExp;
    }

    public int getPlayTime() {
        return playTime;
    }

    public void setPlayTime(int playTime) {
        this.playTime = playTime;
    }

    public String getCurrPokemon() {
        return currPokemon;
    }

    public void setCurrPokemon(String currPokemon) {
        this.currPokemon = currPokemon;
    }

    public int getCurrLevel() {
        return currLevel;
    }

    public void setCurrLevel(int currLevel) {
        this.currLevel = currLevel;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

}
