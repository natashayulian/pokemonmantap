/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import static view.MainLogin.bkgImg;

/**
 *
 * @author natan
 */
public class ChatNPC extends JLabel{

    public ChatNPC(){
        initComponents();
    }
    
    private void initComponents(){

         //chat message npc untuk random
        cMsg = new String[5];
        cMsg[0] = "Hi Player!, have a nice day";
        cMsg[1] = "Did you know, i love summer! ";
        cMsg[2] = "Need help? just ask me!";
        cMsg[3] = "I love to helping others!";
        cMsg[4] = "Hmm, I Think I should stay here for a minute";
        
        Random random = new Random();
        arrayNumber = random.nextInt(5);
        System.out.println();
        
        message = new JLabel(cMsg[arrayNumber]);
        message.setFont(new Font ("Calibri", Font.BOLD, 40));
        message.setLocation(160,450);
        message.setSize(800,100);
        message.setVisible(false);
        bkgImg.add(message);
        
        character = new JLabel();
        character.setIcon(new ImageIcon(resizeImg("img/MainGraph/challenger/npc.png", 70 ,70)));
        character.setSize(70,70);
        character.setLocation(35, 470);
        character.setVisible(false);
        bkgImg.add(character);
        
        chatBox = new JLabel();
        chatBox.setIcon(new ImageIcon(resizeImg("img/MainGraph/Button/panelChat.png", 1025, 250)));
        chatBox.setSize(1025,250);
        chatBox.setLocation(0,330);
        chatBox.setVisible(false);
        bkgImg.add(chatBox);
        
        npcBig = new JLabel();
        npcBig.setIcon(new ImageIcon(resizeImg("img/MainGraph/challenger/npcBig.png", 260,260)));
        npcBig.setSize(260,260);
        npcBig.setLocation(0, 220);
        npcBig.setVisible(false);
        bkgImg.add(npcBig);
        
        //effect gelap
        dimmer = new JLabel();
        dimmer.setIcon(new ImageIcon(resizeImg("img/MainGraph/effect/dimmer.png", 1025, 580)));
        dimmer.setSize(1025, 580);
        dimmer.setLocation(0,0);
        dimmer.setVisible(false);
        bkgImg.add(dimmer);
    }
    
    private Image resizeImg(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);

        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        
        return dimg;
    }
    
    public String [] cMsg;
    public int arrayNumber;
    public JLabel chatBox;
    public JLabel npcBig;
    public JLabel dimmer;
    public JLabel character;
    public JLabel message;
    
}
