/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static model.Player.updateDataPlayerMisc;
import view.BattleAnimation;

/**
 *
 * @author Sasha
 */
public class Pokemon implements PokemonMove {

    private int level;
    private int attack;
    private int hp;
    private int currXp;
    private int damageTotal;

    public Pokemon(int level) {
        this.level = level;
        this.hp = (40 * this.level / 10) + 10 + this.level;
        this.attack = (20 * this.level / 10) + 5 + this.level;
        this.currXp = 0;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
        this.hp = (40 * this.level / 10) + 10 + this.level;
        this.attack = (20 * this.level / 10) + 5 + this.level;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getCurrXp() {
        return currXp;
    }

    public void setCurrXp(int currXp) {
        this.currXp = currXp;
    }

    public void receiveDmg(int dmgReceived) {
        this.setDamageTotal(dmgReceived);
        this.hp = this.hp - dmgReceived;
        if (this.hp < 0) {
            this.hp = 0;
        }

        

    }

    public int getDamageTotal() {
        return damageTotal;
    }

    public void setDamageTotal(int dmgReceived) {
        this.damageTotal = this.getDamageTotal() + dmgReceived;
    }

    @Override
    public void move1(Pokemon target, BattleAnimation animation) {
        Thread t1 = new Thread(animation);
        t1.start();
        target.receiveDmg((this.attack * 3 / 10) + 1);
        System.out.println((this.attack * 3 / 10) + 1 + " dmg");
    }

    @Override
    public void move2(Pokemon target, BattleAnimation animation) {
        Thread t1 = new Thread(animation);
        t1.start();
        target.receiveDmg((this.attack * 5 / 10) + 1);
        System.out.println((this.attack * 5 / 10) + 1 + " dmg");
    }

    @Override
    public void move3(Pokemon target, BattleAnimation animation) {
        Thread t1 = new Thread(animation);
        t1.start();
        target.receiveDmg((this.attack * 4 / 10) + 1);
        System.out.println((this.attack * 4 / 10) + 1 + " dmg");
    }

    @Override
    public void move4(Pokemon target, BattleAnimation animation) {
        Thread t1 = new Thread(animation);
        t1.start();
        target.receiveDmg((this.attack * 1 / 10) + 1);
        System.out.println((this.attack * 1 / 10) + 1 + " dmg");
    }

}
