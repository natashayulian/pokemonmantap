/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import app.DataAccess;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Irvandi Gian
 */
public class Player extends JFrame implements KeyListener {

    JLabel label;
    public static Pokemon pikachu = new Pokemon(5);
    public static DataTrainer dataTrainer = new DataTrainer();
    private int dmgTotal;

    public static void inputDataNewGame(DataTrainer dataMasuk) {
        String name = dataMasuk.getNama();
        dataTrainer.setNama(name);

        System.out.println(dataTrainer.getNama());

    }

    public static void addPlayer() {
        //masuk ke db
        DataAccess.addPlayerStat(dataTrainer);
    }

    public static void updateDataPlayerMisc() {
        dataTrainer.setCurrExp(pikachu.getCurrXp() + dataTrainer.getCurrExp());
        dataTrainer.setCurrLevel(pikachu.getLevel());
        dataTrainer.setDamageTotal(pikachu.getDamageTotal());
        dataTrainer.setCurrPokemon("Pikachu (Default)");
        dataTrainer.setStep(dataTrainer.getStep());


        System.out.println("----------------------");
        System.out.println("nama" + dataTrainer.getNama());
        System.out.println("exp total" + dataTrainer.getCurrExp());
        System.out.println("lvl" + dataTrainer.getCurrLevel());
        System.out.println("dmg" + dataTrainer.getDamageTotal());
        System.out.println("step: " + dataTrainer.getStep());
        System.out.println("------------------------");

        //update ke db
        DataAccess.updatePlayerStat(dataTrainer);
    }
    
    public static void loadDataPlayer(DataTrainer dataGets){
        dataTrainer.setCurrExp(dataGets.getCurrExp());
        dataTrainer.setCurrLevel(dataGets.getCurrLevel());
        dataTrainer.setDamageTotal(dataGets.getDamageTotal());
        dataTrainer.setCurrPokemon(dataGets.getCurrPokemon());
        dataTrainer.setNama(dataGets.getNama());
        dataTrainer.setStep(dataGets.getStep());
    }

    public Player() {
        addKeyListener(this);
        setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

}
