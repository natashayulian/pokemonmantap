/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import static app.DataAccess.addPlayerStat;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import model.DataTrainer;

/**
 *
 * @author natan
 */
public class MainDb {

    public static DataTrainer NewPlayer(DataTrainer dataInput) {
        DataTrainer data = new DataTrainer();
        
        data.setCurrLevel(0);
        data.setCurrPokemon("Pikachu");
        data.setDamageTotal(0);
        data.setNama(data.getNama());
        data.setStep(0);
        data.setCurrExp(0);
        
        //adding
        addPlayerStat(data);
        
        return data;
    }
    
    public static DataTrainer UpdatePlayerData(DataTrainer dataU){
        DataTrainer data = new DataTrainer();
        
        data.setCurrLevel(dataU.getCurrLevel());
        data.setCurrPokemon("Pikachu");
        data.setDamageTotal(dataU.getDamageTotal());
        data.setNama(data.getNama());
        data.setStep(0);
        data.setCurrExp(dataU.getCurrExp());
        
        return data;
    }

    public static void main(String[] args) {

        Connector.getConn();

        
//        DataAccess.updatePlayerStat(UpdatePlayerData());
    }
}
