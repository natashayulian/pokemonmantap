/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Sasha
 */
public class MapArea extends JFrame {
    
    
    public MapArea(){
        initComponents();
        
    }
    
    private void initComponents(){
        this.setSize(400,400);
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.magenta);
        setTitle("Pokemon ITHB");
        
        
        getContentPane().setLayout(null);
        pnlPanel1 = new JPanel();
        pnlPanel1.setBackground(Color.PINK);
        pnlPanel1.setBounds(100,200,100,200);
        add(pnlPanel1);
        
        lblLabel1 = new JLabel("Name : ");
        pnlPanel1.add(lblLabel1);
        
        txtText1 = new JTextField(7);
        pnlPanel1.add(txtText1);
    
        lblIcon = new JLabel();
        lblIcon.setIcon(new ImageIcon ("img/Logo_Baru_ITHB_17_HA.png"));
        pnlPanel1.add(lblIcon);
    }
    
    private JButton btnOK;
    private JPanel pnlPanel1;
    private JLabel lblLabel1;
    private JTextField txtText1;
    private JLabel lblIcon;
}
