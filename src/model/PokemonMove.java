/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import view.BattleAnimation;

/**
 *
 * @author Jason Tan
 */
public interface PokemonMove {
    public void move1(Pokemon target , BattleAnimation animation);
    public void move2(Pokemon target , BattleAnimation animation);
    public void move3(Pokemon target , BattleAnimation animation);
    public void move4(Pokemon target , BattleAnimation animation);
   
}
