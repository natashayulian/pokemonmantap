/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Font;
import javax.swing.JLabel;

/**
 *
 * @author Sasha
 */
public class BlinkingText extends JLabel implements Runnable{

    public BlinkingText() {
        this.setText("<html><font color='white'><bold>PRESS ENTER TO CONTINUE</bold></font></html>");
        setFont(new Font("Comic Sans", Font.PLAIN, 40));
        setBounds(230, 350, 1000, 40);
    }

    @Override
    public void run() {
        while (true) {
            try {
                int i = 0;
                while (true) {
                    if (i % 2 == 0) {
                        this.setVisible(true);
                    } else {
                        this.setVisible(false);
                    }
                    i++;
                    Thread.sleep(300);
                }    
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
