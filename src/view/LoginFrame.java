/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 *
 * @author Sasha
 */
public class LoginFrame extends JFrame implements KeyListener{
    public LoginFrame(){
       initComponents();
    }
   
   
   private void initComponents(){ 
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setSize(1050, 630);
        setLocationRelativeTo(null);
        setTitle("Pokemon Mantap");
        String loginURL = "img/main/splash.png"; 
        Image background = resizeImage(loginURL,1050,600);
        setContentPane(new JLabel(new ImageIcon(background)));
        addKeyListener(this);
        BlinkingText blinkText = new BlinkingText();
        this.add(blinkText);
        Thread t1 = new Thread(blinkText);
        t1.start();
   }
   
    //Image Edit
    private Image resizeImage(String url, int w, int h){
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance (w,h, Image.SCALE_SMOOTH);
        } catch (IOException ex){
            ex.printStackTrace(System.err);
        }
        return dimg;
    }
    
    private JPanel pnlLogin;
    private JLabel lblLogin;
    


    @Override
    public void keyTyped(KeyEvent ke) {
        
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_ENTER){
            setVisible(false);
            MenuAwal main = new MenuAwal();
            main.setVisible(true);
            }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        }
    
    //main musik
    static Clip clip;
    public static void playSound() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("music/caripokemon.wav").getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    
}
   

