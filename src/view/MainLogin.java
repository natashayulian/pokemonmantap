/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Random;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import model.Movement;
import model.Player;
import view.BattleFrame;
import static view.BattleFrame.clip;
import view.BlinkingText;

public class MainLogin extends Player implements Movement {

    public MainLogin(boolean status) {
        initComponents();
        buttonInit();

    }

    protected String urlTrainerDefault = "img/MainGraph/trainer/staywalk.png/";
    private static int x_player = 600;
    private static int y_player = 400;
    private int x_cha = 100;
    private int y_cha = 100;
    private int npc_x = 750;
    private int npc_y = 430;
    boolean status_npc = false;
    private static int jumlahLangkah;

    private void buttonInit() {

        //button text save
        bSaveLabel = new JLabel("Save");
        bSaveLabel.setFont(new Font("Calibri", Font.BOLD, 30));
        bSaveLabel.setLocation(845, 30);
        bSaveLabel.setSize(200, 80);
        bkgImg.add(bSaveLabel);


        //button text exit
        bExitLabel = new JLabel("Exit");
        bExitLabel.setFont(new Font("Calibri", Font.BOLD, 30));
        bExitLabel.setLocation(845, 80);
        bExitLabel.setSize(200, 80);
        bkgImg.add(bExitLabel);
        
        //button text summary
        bSum = new JLabel("Summ");
        bSum.setFont(new Font("Calibri", Font.BOLD, 30));
        bSum.setLocation(845, 130);
        bSum.setSize(200, 80);
        bkgImg.add(bSum);

        //active button
        activeBtn = new JLabel();
        activeBtn.setIcon(new ImageIcon(resizeImg("img/MainGraph/button/button_menu_select.png", 150, 60)));
        activeBtn.setSize(200, 80);
        activeBtn.setVisible(false);
        bkgImg.add(activeBtn);

        //button save
        bSave = new JLabel();
        bSave.setIcon(new ImageIcon(resizeImg("img/MainGraph/button/button_menu.png", 150, 60)));
        bSave.setLocation(800, 30);
        bSave.setSize(200, 80);
        bkgImg.add(bSave);


        //button exit
        bExit = new JLabel();
        bExit.setIcon(new ImageIcon(resizeImg("img/MainGraph/button/button_menu.png", 150, 60)));
        bExit.setLocation(800, 80);
        bExit.setSize(200, 80);
        bkgImg.add(bExit);
        
        //button summary
        sum = new JLabel();
        sum.setIcon(new ImageIcon(resizeImg("img/MainGraph/button/button_menu.png", 150, 60)));
        sum.setLocation(800, 130);
        sum.setSize(200, 80);
        bkgImg.add(sum);

        bSaveLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int input = JOptionPane.showConfirmDialog(null,
                        "Do you want to save data?");
                
                if(input == JOptionPane.YES_OPTION){
                    Player.updateDataPlayerMisc();
                    JOptionPane.showMessageDialog(null, "Data saved");
                } else {
                    System.out.println("save canceled");
                    JOptionPane.showMessageDialog(null, "Data not saved");
                }
                
                System.out.println("saved");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                activeBtn.setLocation(800, 30);
                activeBtn.setVisible(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                activeBtn.setVisible(false);
            }
        });


        bExitLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("exited");
                System.exit(0);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                activeBtn.setLocation(800, 80);
                activeBtn.setVisible(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                activeBtn.setVisible(false);
            }
        });
        
        bSum.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("sum");
                JOptionPane.showMessageDialog(null, "Name : " + dataTrainer.getNama() + "\n Current Level: " +
                        dataTrainer.getCurrLevel() + "\n Current EXP: " + dataTrainer.getCurrExp() +
                        "\n Damage Receive: " + dataTrainer.getDamageTotal() + "\n Current Pokemon: " + dataTrainer.getCurrPokemon() + "\n Total Step: " + dataTrainer.getStep());
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                activeBtn.setLocation(800, 130);
                activeBtn.setVisible(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                activeBtn.setVisible(false);
            }
        });
    }

    public void initComponents() {

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        this.setSize(1050, 630);
        setLocationRelativeTo(null);
        setTitle("Pokemon Mantap");

        //base color
        getContentPane().setBackground(Color.GREEN);

        //panel utama ke frame
        mainPanel = new JPanel();
        add(mainPanel);

        //background ke panel
        bkgImg = new JLabel();
        bkgImg.setIcon(new ImageIcon(resizeImg("img/MainGraph/Land.png", 1025, 580)));
        mainPanel.add(bkgImg);

        //chat npc
        chat = new ChatNPC();
        
        //npc
        npc = new JLabel();
        npc.setIcon(new ImageIcon("img/MainGraph/challenger/npc.png"));
        npc.setSize(50, 50);
        npc.setLocation(npc_x, npc_y);
        bkgImg.add(npc);
        
        //pohonbesar sebelah npc bot
        bigTreeN = new JLabel();
        bigTreeN.setIcon(new ImageIcon(resizeImg("img/MainGraph/misc/big-tree-npc.png", 240, 290)));
        bigTreeN.setSize(406, 600);
        bigTreeN.setLocation(700, 50);
        bkgImg.add(bigTreeN);

        //rumah penduduk1
        house1 = new JLabel();
        house1.setIcon(new ImageIcon(resizeImg("img/MainGraph/house1.png", 269, 190)));
        house1.setSize(270, 190);
        house1.setLocation(350, 135);
        bkgImg.add(house1);
        
        //player
        plyImg = new JLabel();
        plyImg.setIcon(new ImageIcon(urlTrainerDefault));
        plyImg.setSize(70, 70);
        plyImg.setLocation(x_player, y_player);
        this.repaint();
        bkgImg.add(plyImg);
        
        //challengernya
        challenger = new JLabel();
        challenger.setIcon(new ImageIcon("img/MainGraph/challenger/chal1.png"));
        challenger.setSize(50, 50);
        challenger.setLocation(x_cha, y_cha);
        bkgImg.add(challenger);

        //pepohonan 1
        for (int i = 62; i < 286; i += 32) {
            for (int j = 62; j < 190; j += 32) {
                pohon1 = new JLabel();
                pohon1.setIcon(new ImageIcon("img/MainGraph/misc/pohon1.png"));
                pohon1.setSize(35, 35);
                pohon1.setLocation(i, j);
                bkgImg.add(pohon1);
            }
        }

        //pepohonan depan jalan
        for (int i = 62; i < 190; i += 32) {
            pohon1 = new JLabel();
            pohon1.setIcon(new ImageIcon("img/MainGraph/misc/pohon1.png"));
            pohon1.setSize(35, 35);
            pohon1.setLocation(i, 190);
            bkgImg.add(pohon1);

        }
        

        //pager pinggir pakai pohon up
        for (int i = 0; i < 1050; i += 32) {
            pagarPohon = new JLabel();
            pagarPohon.setIcon(new ImageIcon("img/MainGraph/misc/tree1.png"));
            pagarPohon.setSize(35, 35);
            pagarPohon.setLocation(i, 0);
            bkgImg.add(pagarPohon);
        }

        //pager pinggir pakai pohon left
        for (int i = 0; i < 576; i += 32) {
            pagarPohon = new JLabel();
            pagarPohon.setIcon(new ImageIcon("img/MainGraph/misc/tree1.png"));
            pagarPohon.setSize(35, 35);
            pagarPohon.setLocation(0, i);
            bkgImg.add(pagarPohon);
        }

        //pager pinggir pakai pohon right
        for (int i = 0; i < 544; i += 32) {
            pagarPohon = new JLabel();
            pagarPohon.setIcon(new ImageIcon("img/MainGraph/misc/tree1.png"));
            pagarPohon.setSize(35, 35);
            pagarPohon.setLocation(993, i);
            bkgImg.add(pagarPohon);
        }

        //pager pinggir pakai pohon down
        for (int i = 0; i < 256; i += 32) {
            pagarPohon = new JLabel();
            pagarPohon.setIcon(new ImageIcon("img/MainGraph/misc/tree1.png"));
            pagarPohon.setSize(35, 35);
            pagarPohon.setLocation(i, 544);
            bkgImg.add(pagarPohon);

        }

        //pohon-besar
        for (int y = 230; y < 380; y += 50) {
            for (int x = 50; x < 350; x += 50) {
                bigtree = new JLabel();
                bigtree.setIcon(new ImageIcon("img/MainGraph/misc/big-tree.png"));
                bigtree.setSize(50, 50);
                bigtree.setLocation(x, y);
                bkgImg.add(bigtree);
            }
        }

        for (int y = 380; y < 480; y += 50) {
            for (int x = 50; x < 600; x += 50) {
                bigtree = new JLabel();
                bigtree.setIcon(new ImageIcon("img/MainGraph/misc/big-tree.png"));
                bigtree.setSize(50, 50);
                bigtree.setLocation(x, y);
                bkgImg.add(bigtree);
            }
        }

        for (int x = 50; x < 500; x += 50) {
            bigtree = new JLabel();
            bigtree.setIcon(new ImageIcon("img/MainGraph/misc/big-tree.png"));
            bigtree.setSize(50, 50);
            bigtree.setLocation(x, 480);
            bkgImg.add(bigtree);
        }

        //jalan setapak
        for (int i = 600; i < 726; i += 42) {
            jalanSetapak = new JLabel();
            jalanSetapak.setIcon(new ImageIcon(resizeImg("img/MainGraph/jalanan.png", 42, 42)));
            jalanSetapak.setSize(43, 43);
            jalanSetapak.setLocation(i, 446);
            bkgImg.add(jalanSetapak);
        }

        for (int i = 320; i < 488; i += 42) {
            jalanSetapak = new JLabel();
            jalanSetapak.setIcon(new ImageIcon(resizeImg("img/MainGraph/jalanan.png", 42, 42)));
            jalanSetapak.setSize(43, 43);
            jalanSetapak.setLocation(600, i);
            bkgImg.add(jalanSetapak);
        }

        for (int i = 600; i > 320; i -= 42) {
            jalanSetapak = new JLabel();
            jalanSetapak.setIcon(new ImageIcon(resizeImg("img/MainGraph/jalanan.png", 42, 42)));
            jalanSetapak.setSize(43, 43);
            jalanSetapak.setLocation(i, 320);
            bkgImg.add(jalanSetapak);
        }

        for (int i = 320; i > 194; i -= 42) {
            jalanSetapak = new JLabel();
            jalanSetapak.setIcon(new ImageIcon(resizeImg("img/MainGraph/jalanan.png", 42, 42)));
            jalanSetapak.setSize(43, 43);
            jalanSetapak.setLocation(348, i);
            bkgImg.add(jalanSetapak);
        }

        for (int i = 348; i > 180; i -= 42) {
            jalanSetapak = new JLabel();
            jalanSetapak.setIcon(new ImageIcon(resizeImg("img/MainGraph/jalanan.png", 42, 42)));
            jalanSetapak.setSize(43, 43);
            jalanSetapak.setLocation(i, 194);
            bkgImg.add(jalanSetapak);

        }

    }

    private Image resizeImg(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);

        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }

        return dimg;
    }
    
    public void challengerPosition(int x, int y) {
        //batas pohon gede
        if (((x > 20) && (x < 330))&& (y > 180)){
            y = 180;
        }
        if (((x > 320) && (x < 580))&& (y > 340)){
            y = 340;
        }
        
        if (((y > 180) && (y < 340))&& (x < 330)){
            x = 330;
        }
        if (((y > 340) && (y < 460))&& (x < 580)){
            x = 580;
        }

        //bates rumah
        if (((x > 350) && (x < 570)) && ((y < 300) && (y >= 230))) {
            y = 300;
        }
        if (((x > 350) && (x < 570)) && ((y < 230) && (y > 160))) {
            y = 160;
        }
        if (((y > 160) && (y < 290)) && ((x < 575) && (x >= 460))) {
            x = 575;
        }
        if (((y > 160) && (y < 290)) && ((x < 460) && (x > 345))) {
            x = 345;
        }

        //obstacle laut
            //horizontal
        if (((x > 580) && (x < 740)) && (y > 440)) {
            y = 440;
        }
        if (((x > 730) && (x < 840)) && (y > 470)) {
            y = 470;
        }
            //vertikal
        if (((y > 440) && (y < 480)) && (x < 750)) {
            x = 750;
        }
        if (((y > 470) && (y < 510)) && (x < 850)) {
            x = 850;
        }
        challenger.setLocation(x, y);
    }

    //main musik
    static Clip clip;
    
    public static void playSound() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("music/09.wav").getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }

    private JPanel mainPanel;
    public static JLabel bkgImg;
    private static JLabel plyImg;
    private JLabel pohon1;
    private JLabel pagarPohon;
    private JLabel jalanSetapak;
    private JLabel challenger;
    private JLabel house1;
    private JLabel bigtree;
    private JLabel bSave, bExit, bLoad;
    private JLabel bSaveLabel, bExitLabel, bLoadLabel;
    private JLabel activeBtn;
    private JLabel bigTreeN;
    private JLabel npc;
    private ChatNPC chat;
    private JLabel sum,bSum;

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            urlTrainerDefault = "img/MainGraph/trainer/rightwalk2.png/";
            plyImg.setIcon(new ImageIcon(urlTrainerDefault));
//            plyImg.repaint();
            x_player = MoveRight(x_player);
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            urlTrainerDefault = "img/MainGraph/trainer/leftwalk1.png/";
            x_player = MoveLeft(x_player);
        }
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            urlTrainerDefault = "img/MainGraph/trainer/backwalk1.png/";
            y_player = MoveUp(y_player);
        }
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            urlTrainerDefault = "img/MainGraph/trainer/walk1.png/";
            y_player = MoveDown(y_player);
        }
        plyImg.setIcon(new ImageIcon(urlTrainerDefault));
        Obstacle();
        plyImg.setLocation(x_player, y_player);

        if (intersects(plyImg, challenger)) {
            System.out.println("Go Battle !!!");

            BattleFrame battle = new BattleFrame(this);
            BattleFrame.playSound();
            battle.setVisible(true);
            clip.stop();
            this.setVisible(false);
        }

        //bagian npc intersect
        if (intersects(plyImg, npc)) {
            System.out.println("chat");
            
            Random random = new Random();
            chat.message.setText(chat.cMsg[random.nextInt(5)]);
            chat.message.setVisible(true);
            chat.character.setVisible(true);
            chat.chatBox.setVisible(true);
            chat.npcBig.setVisible(true);
            chat.dimmer.setVisible(true);
        } else {
            chat.message.setVisible(false);
            chat.character.setVisible(false);
            chat.chatBox.setVisible(false);
            chat.npcBig.setVisible(false);
            chat.dimmer.setVisible(false);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            urlTrainerDefault = "img/MainGraph/trainer/stayright.png/";
            x_player = MoveRight(x_player);
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            urlTrainerDefault = "img/MainGraph/trainer/stayleft.png/";
            x_player = MoveLeft(x_player);
        }
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            urlTrainerDefault = "img/MainGraph/trainer/stayback.png/";
            y_player = MoveUp(y_player);
        }
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            urlTrainerDefault = "img/MainGraph/trainer/staywalk.png/";
            y_player = MoveDown(y_player);
        }
        plyImg.setIcon(new ImageIcon(urlTrainerDefault));
        System.out.println("----------");
        System.out.println("x_player = " + x_player);
        System.out.println("y_player = " + y_player);
        Obstacle();
        plyImg.setLocation(x_player, y_player);
    }

    public boolean intersects(JLabel lblA, JLabel lblB) {
        Area areaA = new Area(lblA.getBounds());
        Area areaB = new Area(lblB.getBounds());

        return areaA.intersects(areaB.getBounds2D());
    }

    public void Obstacle() {
        //border
        if (x_player < 20) {
            x_player = 20;
        } else if (x_player > 950) {
            x_player = 950;
        }
        if (y_player < 0) {
            y_player = 0;
        } else if (y_player > 500) {
            y_player = 500;
        }

        //batas pohon gede yg kecil
            //horizontal
        if (((x_player > 320) && (x_player < 570)) && (y_player > 320)) {
            y_player = 320;
        }
        if (((x_player > 40) && (x_player < 320)) && (y_player > 170)) {
            y_player = 170;
        }
            //vertikal
        if (((y_player > 180) && (y_player < 340)) && (x_player < 330)) {
            x_player = 330;
        }
        if (((y_player > 340) && (y_player < 460)) && (x_player < 580)) {
            x_player = 580;
        }
        
        //bates rumah
        if (((x_player > 350) && (x_player < 570)) && ((y_player < 300) && (y_player >= 230))) {
            y_player = 300;
        }
        if (((x_player > 350) && (x_player < 570)) && ((y_player < 230) && (y_player > 160))) {
            y_player = 160;
        }
        if (((y_player > 160) && (y_player < 290)) && ((x_player < 575) && (x_player >= 460))) {
            x_player = 575;
        }
        if (((y_player > 160) && (y_player < 290)) && ((x_player < 460) && (x_player > 345))) {
            x_player = 345;
        }
        
        //obstacle laut
            //horizontal
        if (((x_player > 580) && (x_player < 740)) && (y_player > 440)) {
            y_player = 440;
        }
        if (((x_player > 730) && (x_player < 840)) && (y_player > 470)) {
            y_player = 470;
        }
            //vertikal
        if (((y_player > 440) && (y_player < 480)) && (x_player < 750)) {
            x_player = 750;
        }
        if (((y_player > 470) && (y_player < 510)) && (x_player < 850)) {
            x_player = 850;
        }

    }

    @Override
    public int MoveUp(int y) {
        y -= 5;
        jumlahLangkah += 1;
        dataTrainer.setStep(jumlahLangkah);
        System.out.println("Jumlah Langkah Player : " + jumlahLangkah);
        return y;
    }

    @Override
    public int MoveDown(int y) {
        y += 5;
        jumlahLangkah += 1;
        dataTrainer.setStep(jumlahLangkah);
        System.out.println("Jumlah Langkah Player : " + jumlahLangkah);
        return y;
    }

    @Override
    public int MoveLeft(int x) {
        x -= 5;
        jumlahLangkah += 1;
        dataTrainer.setStep(jumlahLangkah);
        System.out.println("Jumlah Langkah Player : " + jumlahLangkah);
        return x;
    }

    @Override
    public int MoveRight(int x) {
        x += 5;
        jumlahLangkah += 1;
        dataTrainer.setStep(jumlahLangkah);
        System.out.println("Jumlah Langkah Player : " + jumlahLangkah);
        return x;
    }

    

}
