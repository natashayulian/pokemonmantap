
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Sasha
 */
public class MapBattle extends JFrame implements ActionListener{
    public MapBattle() {
        initComponents();
    }
    
    private void initComponents(){
        //layar
        //getContentPane().setLayout(null);
        setSize(600,600);
        setLocationRelativeTo(null);
        setTitle("Frame Battle");
        
        //create panel
        pnlPanel1 = new JPanel();
        add(pnlPanel1);
              
        //background base
        lblBackground = new JLabel();
        lblBackground.setIcon (new ImageIcon (resizeImg("img/battle.jpg",600,600)));
        pnlPanel1.add(lblBackground);
        
        //pokemon pikachu
        lblPikachu = new JLabel();
        lblPikachu.setIcon (new ImageIcon (resizeImg ("img/pikachu.jpg",120,120)));
        lblPikachu.setLocation(100,400);
        lblPikachu.setSize(120,120);
        lblBackground.add(lblPikachu);
        
        //pokemon musuh
        lblEnemy = new JLabel();
        lblEnemy.setIcon (new ImageIcon (resizeImg ("img/pidgey.jpg",120,120)));
        lblEnemy.setLocation(300,500);
        lblEnemy.setSize(120,120);
        lblBackground.add(lblEnemy);
    }
    
    
    private Image resizeImg(String url, int w, int h){
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance (w,h, Image.SCALE_SMOOTH);
        } catch (IOException ex){
            ex.printStackTrace(System.err);
        }
        return dimg;
    }
    
    public static void main(String[] args) {
        new MapBattle().setVisible(true);
        
    }
    
    private JPanel pnlPanel1;
    private JLabel lblBackground;
    private JLabel lblPikachu;
    private JLabel lblEnemy;
    

    @Override
    public void actionPerformed(ActionEvent ae) {
        
    }
}
